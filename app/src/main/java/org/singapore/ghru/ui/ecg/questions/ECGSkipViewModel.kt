package org.singapore.ghru.ui.ecg.questions

import androidx.lifecycle.ViewModel

const val TYPE_BP = "bp"
const val TYPE_ECG = "ecg"
const val TYPE_FUNDO = "fundo"
const val TYPE_ULTRASOUND = "ultrasound"
const val TYPE_TREADMILL = "treadmill"

class ECGSkipViewModel : ViewModel() {

}
