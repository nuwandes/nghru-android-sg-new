package org.singapore.ghru.ui.gripstrangth.contraindication

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class GripStrengthQuestionnaireViewModel
@Inject constructor() : ViewModel() {

    val havePlaster = MutableLiveData<Boolean>()
    val haveJoint = MutableLiveData<Boolean>()

    fun setHavePlaster(item: Boolean) {
        havePlaster.value = item
    }

    fun setHaveJoint(item: Boolean) {
        haveJoint.value = item
    }

}
