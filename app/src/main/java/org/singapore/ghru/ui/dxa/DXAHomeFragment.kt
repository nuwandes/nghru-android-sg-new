package org.singapore.ghru.ui.dxa

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.birbit.android.jobqueue.JobManager
import com.crashlytics.android.Crashlytics
import com.google.gson.GsonBuilder

import org.singapore.ghru.R
import org.singapore.ghru.binding.FragmentDataBindingComponent
import org.singapore.ghru.databinding.DXAHomeFragmentBinding
import org.singapore.ghru.di.Injectable
import org.singapore.ghru.ui.dxa.completed.CompletedDialogFragment
import org.singapore.ghru.ui.dxa.reason.ReasonDialogFragment
import org.singapore.ghru.util.autoCleared
import org.singapore.ghru.util.getLocalTimeString
import org.singapore.ghru.util.singleClick
import org.singapore.ghru.vo.Measurements
import org.singapore.ghru.vo.StationDeviceData
import org.singapore.ghru.vo.Status
import org.singapore.ghru.vo.request.DXABody
import org.singapore.ghru.vo.request.DXABodyData
import org.singapore.ghru.vo.request.ParticipantRequest
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class DXAHomeFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var binding by autoCleared<DXAHomeFragmentBinding>()
    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    @Inject
    lateinit var viewModel: DXAHomeViewModel

    private var participantRequest: ParticipantRequest? = null

    private var deviceListName: MutableList<String> = arrayListOf()
    private var deviceListObject: List<StationDeviceData> = arrayListOf()
    private var selectedDeviceID: String? = null

    private var whole_body: String? = null
    private var lumbar_spine: String? = null
    private var hip: String? = null
    private var hip_used: String? = null

    @Inject
    lateinit var jobManager: JobManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            participantRequest = arguments?.getParcelable<ParticipantRequest>("ParticipantRequest")!!
        } catch (e: KotlinNullPointerException) {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil.inflate<DXAHomeFragmentBinding>(
            inflater,
            R.layout.d_x_a_home_fragment,
            container,
            false
        )
        binding = dataBinding

        setHasOptionsMenu(true)
        val appCompatActivity = requireActivity() as AppCompatActivity
        appCompatActivity.setSupportActionBar(binding.detailToolbar)
        appCompatActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.setLifecycleOwner(this)
        binding.viewModel = viewModel
        binding.participant = participantRequest

        deviceListName.clear()
        deviceListName.add(getString(R.string.unknown))
        val adapter = ArrayAdapter(context!!, R.layout.basic_spinner_dropdown_item, deviceListName)
        binding.deviceIdSpinner.setAdapter(adapter);

        viewModel.setStationName(Measurements.DXA)
        viewModel.stationDeviceList?.observe(this, Observer {
            if (it.status.equals(Status.SUCCESS)) {
                deviceListObject = it.data!!

                deviceListObject.iterator().forEach {
                    deviceListName.add(it.device_name!!)
                }
                adapter.notifyDataSetChanged()
            }
        })
        binding.deviceIdSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, @NonNull selectedItemView: View?, position: Int, id: Long) {
                if (position == 0) {
                    selectedDeviceID = null
                } else {
                    binding.textViewDeviceError.visibility = View.GONE
                    selectedDeviceID = deviceListObject[position - 1].device_id
                }

            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // your code here
            }

        }

        viewModel.dxaComplete?.observe(this, Observer { participant ->

            if (participant?.status == Status.SUCCESS) {
                val completedDialogFragment = CompletedDialogFragment()
                completedDialogFragment.arguments = bundleOf("is_cancel" to false)
                completedDialogFragment.show(fragmentManager!!)
            } else if (participant?.status == Status.ERROR) {
                Crashlytics.setString("comment", binding.comment.text.toString())
                Crashlytics.setString("participant", participant.toString())
                Crashlytics.logException(Exception("dxaComplete " + participant.message.toString()))
                binding.executePendingBindings()
            }
        })

        binding.nextButton.singleClick {
            if(selectedDeviceID==null) {
                binding.textViewDeviceError.visibility = View.VISIBLE
            }
            else if (validateDXA()) {
                val endTime: String = convertTimeTo24Hours()
                val endDate: String = getDate()
                val endDateTime:String = endDate + " " + endTime

                participantRequest?.meta?.endTime =  endDateTime

                    val mDXABody = DXABody(DXABodyData(binding.comment.text.toString(),selectedDeviceID!!,whole_body!!, lumbar_spine!!, hip!!, hip_used!!))
                    val gson = GsonBuilder().setPrettyPrinting().create()
                viewModel.setParticipantComplete(
                    participantRequest!!, isNetworkAvailable(),
                    gson.toJson(mDXABody)
                )
            } else {

            }
        }

        binding.buttonCancel.singleClick {
            val reasonDialogFragment = ReasonDialogFragment()
            reasonDialogFragment.arguments = bundleOf("ParticipantRequest" to participantRequest)
            reasonDialogFragment.show(fragmentManager!!)
        }

        binding.radioGroupWholeBody.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.yesWholeBody) {
                whole_body = "yes"
                binding.radioGroupWholeBodyValue = false;
            } else {
                whole_body = "no"
                binding.radioGroupWholeBodyValue = false;
            }
            binding.executePendingBindings()
        }
        binding.radioGroupLumbar.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.yesLumbar) {
                lumbar_spine = "yes"
                binding.radioGroupLumbarValue = false;
            } else {
                lumbar_spine = "no"
                binding.radioGroupLumbarValue = false;
            }
            binding.executePendingBindings()
        }
        binding.radioGroupHip.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.yesHip) {
                hip = "yes"
                binding.radioGroupHipValue = false;
            } else {
                hip = "no"
                binding.radioGroupHipValue = false;
            }
            binding.executePendingBindings()
        }
        binding.radioGroupHipUsed.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.leftHipUsed) {
                hip_used = "left"
                binding.radioGroupHipUsedValue = false;
            } else {
                hip_used = "right"
                binding.radioGroupHipUsedValue = false;
            }
            binding.executePendingBindings()
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = activity?.getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    private fun validateDXA(): Boolean {
        if(whole_body == null) {
            binding.radioGroupWholeBodyValue = true
            binding.executePendingBindings()
            return false

        }
        else if(lumbar_spine == null) {
            binding.radioGroupLumbarValue = true
            binding.executePendingBindings()
            return false
        }
        else if(hip == null) {
            binding.radioGroupHipValue = true
            binding.executePendingBindings()
            return false
        }
        else if(hip_used == null) {
            binding.radioGroupHipUsedValue = true
            binding.executePendingBindings()
            return false
        }

        return true
    }

    private fun convertTimeTo24Hours(): String {
        val now: Calendar = Calendar.getInstance()
        val inputFormat: DateFormat = SimpleDateFormat("MMM DD, yyyy HH:mm:ss")
        val outputformat: DateFormat = SimpleDateFormat("HH:mm")
        val date: Date
        val output: String
        try{
            date= inputFormat.parse(now.time.toLocaleString())
            output = outputformat.format(date)
            return output
        }catch(p: ParseException){
            return ""
        }
    }

    private fun getDate(): String {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm")
        val outputformat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        val date: Date
        val output: String
        try{
            date= inputFormat.parse(binding.root.getLocalTimeString())
            output = outputformat.format(date)

            return output
        }catch(p: ParseException){
            return ""
        }
    }

    /**
     * Created to be able to override in tests
     */
    fun navController() = findNavController()

}
