package org.singapore.ghru.ui.foodquestionnaire.guide

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController

import org.singapore.ghru.R
import org.singapore.ghru.binding.FragmentDataBindingComponent
import org.singapore.ghru.databinding.FfqGuideFragmentBinding
import org.singapore.ghru.di.Injectable
import org.singapore.ghru.event.BusProvider
import org.singapore.ghru.ui.foodquestionnaire.contraindication.FoodFrequencyQuestionnaireViewModel
import org.singapore.ghru.ui.heightweight.errordialog.ErrorDialogFragment
import org.singapore.ghru.util.autoCleared
import org.singapore.ghru.util.hideKeyboard
import org.singapore.ghru.util.singleClick
import org.singapore.ghru.vo.ParticipantCre
import org.singapore.ghru.vo.Status
import org.singapore.ghru.vo.request.ParticipantRequest
import javax.inject.Inject

class FoodFrequencyGuideFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var binding by autoCleared<FfqGuideFragmentBinding>()
    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    @Inject
    lateinit var viewModel: FoodFrequencyGuideViewModel

    private var participantRequest: ParticipantRequest? = null

    private var participantData: ParticipantCre? = null

    private lateinit var questionnaireViewModel: FoodFrequencyQuestionnaireViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            participantRequest = arguments?.getParcelable<ParticipantRequest>("ParticipantRequest")!!
        } catch (e: KotlinNullPointerException) {
            //Crashlytics.logException(e)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil.inflate<FfqGuideFragmentBinding>(
            inflater,
            R.layout.ffq_guide_fragment,
            container,
            false
        )
        binding = dataBinding

        //setHasOptionsMenu(true)
        val appCompatActivity = requireActivity() as AppCompatActivity
        appCompatActivity.setSupportActionBar(binding.detailToolbar)
        appCompatActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        return dataBinding.root
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        viewModel = activity?.run {
//            ViewModelProviders.of(this).get(FoodFrequencyGuideViewModel::class.java)
//        } ?: throw Exception("Invalid Activity")
//    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.participant = participantRequest
        binding.setLifecycleOwner(this)

        binding.nextButton.singleClick {
            if (validateNextButton()) {
                val bundle = Bundle()
                bundle.putParcelable("ParticipantRequest", participantRequest)
                navController().navigate(R.id.action_FFQ_GuideFragment_to_ConfirmationFragment, bundle)
            }
        }

        binding.refreshButton.singleClick {

            viewModel.setScreeningId("AAA")

            viewModel.getParticipantCredintials.observe(this, Observer { participantResource ->

                if (participantResource?.status == Status.SUCCESS) {
                    participantData = participantResource.data!!.data!!

                    if (participantData != null)
                    {
                        binding.ffqLoginId.setText(participantData!!.username)
                        binding.ffqPassword.setText(participantData!!.password)
                        viewModel.setHaveCredintials(true)
                    }
                } else if (participantResource?.status == Status.ERROR) {
                    val errorDialogFragment = ErrorDialogFragment()
                    errorDialogFragment.setErrorMessage(participantResource.message!!.data!!.message!!)
                    errorDialogFragment.show(fragmentManager!!)
                    //Crashlytics.logException(Exception(participantResource.toString()))
                }
                binding.executePendingBindings()


            })

            viewModel.setScreeningId(participantRequest!!.screeningId)
        }

        viewModel.setScreeningId(participantRequest!!.screeningId)

        viewModel.getParticipantCredintials.observe(this, Observer { participantResource ->

            if (participantResource?.status == Status.SUCCESS) {
                participantData = participantResource.data!!.data!!

                if (participantData != null)
                {
                    binding.ffqLoginId.setText(participantData!!.username)
                    binding.ffqPassword.setText(participantData!!.password)
                    viewModel.setHaveCredintials(true)
                }
            } else if (participantResource?.status == Status.ERROR) {
                val errorDialogFragment = ErrorDialogFragment()
                errorDialogFragment.setErrorMessage(participantResource.message!!.data!!.message!!)
                errorDialogFragment.show(fragmentManager!!)
                //Crashlytics.logException(Exception(participantResource.toString()))
            }
            binding.executePendingBindings()
        })
    }

    private fun validateNextButton(): Boolean {
        if(viewModel.haveCredentials.value == null) {
            binding.isCredentialsValue = true
            binding.executePendingBindings()
            return false
        }

        return true
    }

    /**
     * Created to be able to override in tests
     */
    fun navController() = findNavController()

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                binding.root.hideKeyboard()
                navController().popBackStack()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        questionnaireViewModel = activity?.run {
            ViewModelProviders.of(this).get(FoodFrequencyQuestionnaireViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    private fun getContraindications(): MutableList<Map<String, String>> {
        val contraindications = mutableListOf<Map<String, String>>()

        val haveCommunicate = questionnaireViewModel.haveCommunicate.value
        val communicationMap = mutableMapOf<String, String>()
        communicationMap["question"] = "Is participant able to communicate answers?"
        communicationMap["answer"] = if (haveCommunicate!!) "yes" else "no"

        contraindications.add(communicationMap)

        return contraindications
    }

    override fun onResume() {
        super.onResume()
        BusProvider.getInstance().register(this)
    }

    override fun onPause() {
        super.onPause()
        BusProvider.getInstance().unregister(this)
    }

}
