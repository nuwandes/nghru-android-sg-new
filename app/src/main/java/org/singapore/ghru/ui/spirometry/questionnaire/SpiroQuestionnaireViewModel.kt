package org.singapore.ghru.ui.spirometry.questionnaire

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SpiroQuestionnaireViewModel : ViewModel() {

    val haveRetina = MutableLiveData<String>()
    val haveEyeSurgery = MutableLiveData<String>()
    val haveOtherSurgery = MutableLiveData<String>()
    val haveMyocardialInfarction = MutableLiveData<Boolean>()
    val haveSufferedStroke = MutableLiveData<Boolean>()
    val haveChestInfection = MutableLiveData<Boolean>()
    val haveTuberculosis = MutableLiveData<Boolean>()
    val havePneumothorax = MutableLiveData<Boolean>()

    fun setHaveRetina(item: String) {
        haveRetina.value = item
    }

    fun setHaveEyeSurgery(item: String) {
        haveEyeSurgery.value = item
    }

    fun setHaveOtherSurgery(item: String) {
        haveOtherSurgery.value = item
    }

    fun setHaveMycardial(item: Boolean) {
        haveMyocardialInfarction.value = item
    }

    fun setHaveStroke(item: Boolean) {
        haveSufferedStroke.value = item
    }

    fun setHaveChestInfection(item: Boolean) {
        haveChestInfection.value = item
    }

    fun setHaveTuberculosis(item: Boolean) {
        haveTuberculosis.value = item
    }

    fun setHavePneumothorax(item: Boolean) {
        havePneumothorax.value = item
    }

}
