package org.singapore.ghru.ui.gripstrangth


import android.content.Context
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.birbit.android.jobqueue.JobManager
import com.crashlytics.android.Crashlytics
import io.reactivex.disposables.CompositeDisposable
import org.singapore.ghru.R
import org.singapore.ghru.binding.FragmentDataBindingComponent
import org.singapore.ghru.databinding.GripStrengthHomeFragmentBinding
import org.singapore.ghru.di.Injectable
import org.singapore.ghru.event.*
import org.singapore.ghru.ui.gripstrangth.contraindication.GripStrengthQuestionnaireViewModel
import org.singapore.ghru.ui.heightweight.completed.CompletedDialogFragment
import org.singapore.ghru.ui.heightweight.contraindication.HeightWeightQuestionnaireViewModel
import org.singapore.ghru.ui.gripstrangth.reason.ReasonDialogFragment
import org.singapore.ghru.util.*
import org.singapore.ghru.vo.*
import org.singapore.ghru.vo.request.*
import org.singapore.ghru.vo.request.BodyMeasurement
import timber.log.Timber
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.Date
import javax.inject.Inject
import kotlin.collections.ArrayList


class GripStrengthHomeFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var binding: GripStrengthHomeFragmentBinding


    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    @Inject
    lateinit var viewModel: GripStrengthHomeViewModel

    private var sampleRequest: SampleRequest? = null

    private val disposables = CompositeDisposable()

    @Inject
    lateinit var jobManager: JobManager


    private var leftGrip: GripStrengthData? = null
    private var rightGrip: GripStrengthData? = null
    private var gripStrength: GripStrengthTests? = null

    private var participantRequest: ParticipantRequest? = null

    var user: User? = null
    var meta: Meta? = null

    private lateinit var questionnaireViewModel: GripStrengthQuestionnaireViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            participantRequest = arguments?.getParcelable<ParticipantRequest>("ParticipantRequest")!!

        } catch (e: KotlinNullPointerException) {
            //Crashlytics.logException(e)
        }

//        disposables.add(
//            BodyMeasurementDataRxBus.getInstance().toObservable()
//                .subscribe({ result ->
//                    // if (result == null) {
//                    Timber.d(result.bodyMeasurementData.toString())
//                    when (result.eventType) {
//                        BodyMeasurementDataEventType.HEIGHT -> {
//                            height = result.bodyMeasurementData
//                            navController().popBackStack()
//                            binding.linearLayoutGripx.visibility = View.VISIBLE
//                        }
//                        BodyMeasurementDataEventType.HIP_WAIST -> {
//                            hipWaist = result.bodyMeasurementData
//                            navController().popBackStack()
//                            binding.linearLayoutHipWaistX.visibility = View.VISIBLE
//                        }
//                        BodyMeasurementDataEventType.BODY_COMOSITION -> {
//                            bodyComposition = result.bodyMeasurementData
//                            navController().popBackStack()
//                            binding.linearLayoutStrengthX.visibility = View.VISIBLE
//                        }
//
//                    }
//                }, { error ->
//                    print(error)
//                    error.printStackTrace()
//                })
//        )

        disposables.add(
            LeftGripRecordTestRxBus.getInstance().toObservable()
                .subscribe({ result ->

                    Timber.d(result.toString())
                    leftGrip = result

                }, { error ->
                    print(error)
                    error.printStackTrace()
                })
        )

        disposables.add(
            RightGripRecordTestRxBus.getInstance().toObservable()
                .subscribe({ result ->

                    Timber.d(result.toString())
                    rightGrip = result

                }, { error ->
                    print(error)
                    error.printStackTrace()
                })
        )
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil.inflate<GripStrengthHomeFragmentBinding>(
            inflater,
            R.layout.grip_strength_home_fragment,
            container,
            false
        )
        binding = dataBinding
        setHasOptionsMenu(true)
        val appCompatActivity = requireActivity() as AppCompatActivity
        appCompatActivity.setSupportActionBar(binding.detailToolbar)
        appCompatActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.root.hideKeyboard()
        return dataBinding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.setLifecycleOwner(this)
        binding.viewModel = viewModel
        binding.sample = sampleRequest
        binding.participant = participantRequest

        viewModel.setUser("user")
        viewModel.user?.observe(this, Observer { userData ->
            if (userData?.data != null) {
                // setupNavigationDrawer(userData.data)
                user = userData.data

                val sTime: String = convertTimeTo24Hours()
                val sDate: String = getDate()
                val sDateTime:String = sDate + " " + sTime

                meta = Meta(collectedBy = user?.id, startTime = sDateTime)
                //meta?.registeredBy = user?.id
            }

        })

        if (leftGrip != null) {
                binding.gripCompleteView.visibility = View.VISIBLE
                binding.linearLayoutGrip.background =
                    resources.getDrawable(R.drawable.ic_process_complete_bg, null)
        }

        if (rightGrip != null) {
                binding.strengthCompleteView.visibility = View.VISIBLE
                binding.linearLayoutStrength.background =
                    resources.getDrawable(R.drawable.ic_process_complete_bg, null)
        }

        Log.d("GRIP_STRENGTH_HOME","DATA:" + leftGrip.toString()  + ", AND " + rightGrip)
        binding.errorView.collapse()

        viewModel.sampleMangementPocess?.observe(this, Observer { sampleMangementPocess ->
            // Timber.d(sampleMangementPocess.toString())
            if (sampleMangementPocess?.status == Status.SUCCESS) {
                activity!!.finish()
            } else if (sampleMangementPocess?.status == Status.ERROR) {
                binding.progressBar.visibility = View.GONE
                binding.buttonSubmit.visibility = View.VISIBLE
            }
        })

        viewModel.gripMeasurementMetaOffline?.observe(this, Observer { sampleMangementPocess ->

            if(sampleMangementPocess?.status == Status.LOADING){
                binding.progressBar.visibility = View.VISIBLE
                binding.buttonSubmit.visibility = View.GONE
            }else{
                binding.progressBar.visibility = View.GONE
                binding.buttonSubmit.visibility = View.VISIBLE
            }

            if (sampleMangementPocess?.status == Status.SUCCESS) {
                val completedDialogFragment = CompletedDialogFragment()
                completedDialogFragment.arguments = bundleOf("is_cancel" to false)
                completedDialogFragment.show(fragmentManager!!)
                Log.d("ok","ok")
            } else if(sampleMangementPocess?.status == Status.ERROR){
                Crashlytics.setString(
                    "HeightWeightMeasurementMeta",
                    GripStrengthRequest(meta = meta, body = gripStrength).toString()
                )
                Crashlytics.setString("participant", participantRequest.toString())
                Crashlytics.logException(Exception("BodyMeasurementMeta " + sampleMangementPocess.message.toString()))
            }
        })




        binding.buttonCancel.singleClick {
            val reasonDialogFragment = ReasonDialogFragment()
            reasonDialogFragment.arguments = bundleOf("participant" to participantRequest)
            reasonDialogFragment.show(fragmentManager!!)
        }

        binding.buttonSubmit.singleClick {

            if (leftGrip != null && rightGrip != null) {
                gripStrength = GripStrengthTests(leftGrip = leftGrip, rightGrip = rightGrip )
                gripStrength!!.contraindications = getContraindications()

                val endTime: String = convertTimeTo24Hours()
                val endDate: String = getDate()
                val endDateTime:String = endDate + " " + endTime

                meta?.endTime = endDateTime

                val gripStrengthRequest = GripStrengthRequest(meta = meta, body = gripStrength)
                gripStrengthRequest.screeningId = participantRequest?.screeningId!!
                if(isNetworkAvailable()){
                    gripStrengthRequest.syncPending =false
                }else{
                    gripStrengthRequest.syncPending =true

                }

                viewModel.setGripMeasurementMeta(gripStrengthRequest)

                binding.progressBar.visibility = View.VISIBLE
                binding.buttonSubmit.visibility = View.GONE

            } else {
                binding.errorView.expand()
                binding.sampleValidationError = true
                if (leftGrip == null) {
                    updateProcessErrorUI(binding.gripTextView)
                }

                if (rightGrip == null) {
                    updateProcessErrorUI(binding.strengthTextView)

                }
                binding.executePendingBindings()
            }
        }

        binding.linearLayoutGrip.singleClick {

            binding.sampleValidationError = false
            updateProcessValidUI(binding.gripTextView)
            updateProcessValidUI(binding.strengthTextView)
            navController().navigate(R.id.action_GripStrengthHomeFragment_to_GripFragment)
        }



        binding.linearLayoutStrength.singleClick {
            binding.sampleValidationError = false
            updateProcessValidUI(binding.gripTextView)
            updateProcessValidUI(binding.strengthTextView)
            navController().navigate(R.id.action_GripStrengthHomeFragment_to_StrengthFragment)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        questionnaireViewModel = activity?.run {
            ViewModelProviders.of(this).get(GripStrengthQuestionnaireViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    private fun getContraindications(): MutableList<Map<String, String>> {
        val contraindications: MutableList<Map<String, String>> = mutableListOf()

        val havePlaster = questionnaireViewModel.havePlaster.value

        val plasterMap = mutableMapOf<String, String>()
        plasterMap["question"] = "Arm in plaster or recent fracture /injury?"
        plasterMap["answer"] = if (havePlaster!!) "yes" else "no"
        contraindications.add(plasterMap)

        val haveJoint = questionnaireViewModel.haveJoint.value

        val jointMap = mutableMapOf<String, String>()
        jointMap["question"] = "Severe joint condition (wrist and/or fingers, e.g. rheumatoid arthritis?"
        jointMap["answer"] = if (haveJoint!!) "yes" else "no"
        contraindications.add(jointMap)

        return contraindications
    }

    private fun updateProcessErrorUI(view: TextView) {
        view.setTextColor(Color.parseColor("#FF5E45"))
        view.setDrawbleLeftColor("#FF5E45")
    }

    private fun updateProcessValidUI(view: TextView) {
        view.setTextColor(Color.parseColor("#00548F"))
        view.setDrawbleLeftColor("#00548F")
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = activity?.getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }


    override fun onResume() {
        super.onResume()
        BusProvider.getInstance().register(this)
    }

    override fun onPause() {
        super.onPause()
        BusProvider.getInstance().unregister(this)
    }

    private fun convertTimeTo24Hours(): String
    {
        val now: Calendar = Calendar.getInstance()
        val inputFormat: DateFormat = SimpleDateFormat("MMM DD, yyyy HH:mm:ss")
        val outputformat: DateFormat = SimpleDateFormat("HH:mm")
        val date: Date
        val output: String
        try{
            date= inputFormat.parse(now.time.toLocaleString())
            output = outputformat.format(date)
            return output
        }catch(p: ParseException){
            return ""
        }
    }

    private fun getDate(): String
    {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm")
        val outputformat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        val date: Date
        val output: String
        try{
            date= inputFormat.parse(binding.root.getLocalTimeString())
            output = outputformat.format(date)

            return output
        }catch(p: ParseException){
            return ""
        }
    }

    /**
     * Created to be able to override in tests
     */
    fun navController() = findNavController()


}
