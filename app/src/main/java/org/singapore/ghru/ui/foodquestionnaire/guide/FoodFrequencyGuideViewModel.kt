package org.singapore.ghru.ui.foodquestionnaire.guide

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import org.singapore.ghru.repository.FFQRepository
import org.singapore.ghru.util.AbsentLiveData
import org.singapore.ghru.vo.Message
import org.singapore.ghru.vo.ParticipantCre
import org.singapore.ghru.vo.Resource
import org.singapore.ghru.vo.ResourceData
import org.singapore.ghru.vo.request.ParticipantRequest
import javax.inject.Inject

class FoodFrequencyGuideViewModel
@Inject constructor(ffqRepository : FFQRepository) : ViewModel() {

    val haveCredentials = MutableLiveData<Boolean>()

    fun setHaveCredintials(item: Boolean) {
        haveCredentials.value = item
    }

    private val _screeningId: MutableLiveData<String> = MutableLiveData()
    val screeningId: LiveData<String>
        get() = _screeningId

    var getParticipantCredintials: LiveData<Resource<ResourceData<ParticipantCre>>> = Transformations
        .switchMap(_screeningId) { screeningId ->
            if (screeningId == null) {
                AbsentLiveData.create()
            } else {
                ffqRepository.getParticipantCredintials(screeningId)
            }
        }

    fun setScreeningId(screeningId: String?) {
        if (_screeningId.value == screeningId) {
            return
        }
        _screeningId.value = screeningId
    }

}
