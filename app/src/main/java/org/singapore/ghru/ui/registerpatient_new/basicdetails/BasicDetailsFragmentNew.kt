package org.singapore.ghru.ui.registerpatient_new.basicdetails

import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import br.com.ilhasoft.support.validation.Validator
import org.singapore.ghru.R
import org.singapore.ghru.binding.FragmentDataBindingComponent
import org.singapore.ghru.databinding.BasicDetailsFragmentNewBinding
import org.singapore.ghru.databinding.BasicDetailsFragmentSgBinding
import org.singapore.ghru.di.Injectable
import org.singapore.ghru.ui.codeheck.CodeCheckDialogFragment
import org.singapore.ghru.util.*
import org.singapore.ghru.vo.*
import org.singapore.ghru.vo.Date
import org.singapore.ghru.vo.request.*
import org.singapore.ghru.vo.request.Member
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class BasicDetailsFragmentNew : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var binding by autoCleared<BasicDetailsFragmentNewBinding>()

    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    @Inject
    lateinit var basicDetailsViewModelNew: BasicDetailsViewModelNew

    private var member: Member? = null

    private var householdId: String? = null

    val sdf = SimpleDateFormat(Constants.dataFormat, Locale.US)

    var cal = Calendar.getInstance()

    var user: User? = null
    var userConfig: UserConfig? = null

    var meta: Meta? = null
    var hoursFasted: String? = null
    var memberRequest: MemberRequest = MemberRequest.build()

    lateinit var participantMeta: ParticipantMeta

    var household: HouseholdRequest? = null

    private var concentPhoto: String? = null

    private var selectedRelationShip: String? = null

    private lateinit var validator: Validator

    private var screeningIDValid: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            hoursFasted = "8"

            if(arguments?.getString("screeningId") != null) {
                memberRequest.screeningId = arguments?.getString("screeningId")!!
                screeningIDValid = true
                memberRequest.gender = arguments?.getString("gender")!!
                memberRequest.age.dob = arguments?.getString("dob")!!
            }

        } catch (e: KotlinNullPointerException) {
            print(e)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil.inflate<BasicDetailsFragmentNewBinding>(
            inflater,
            R.layout.basic_details_fragment_new,
            container,
            false
        )
        binding = dataBinding
        setHasOptionsMenu(true)
        validator = Validator(binding)
        val appCompatActivity = requireActivity() as AppCompatActivity
        appCompatActivity.setSupportActionBar(binding.detailToolbar)
        appCompatActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.root.hideKeyboard()
        return dataBinding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.setLifecycleOwner(this)

        binding.editTextCode.filters = binding.editTextCode.filters + InputFilter.AllCaps()

        basicDetailsViewModelNew.setUser("user")
        basicDetailsViewModelNew.user?.observe(this, Observer { userData ->
            if (userData?.data != null) {
                user = userData.data

                val stTime: String = convertTimeTo24Hours()
                val stDate: String = getDate()
                val stDateTime:String = stDate + " " + stTime

                meta = Meta(collectedBy = user?.id, startTime = stDateTime)
                meta?.registeredBy = user?.id
            }

        })

        binding.memberRequest = memberRequest
        if (member != null) {
            binding.member = member
            memberRequest.firstName = member?.name!!
            memberRequest.lastName = member?.familyName!!
            memberRequest.nickName = if (member?.nickName == null) "" else member?.nickName!!
            memberRequest.gender = member?.gender!!
            memberRequest.hoursFasted = hoursFasted?.toInt()!!
            memberRequest.contactDetails.phoneNumberPreferred =
                    if (member?.contactNo == null) "" else member?.contactNo!!
            memberRequest.age.ageInYears = member?.age!!
            memberRequest.age.dob = member?.birthDate?.year.toString() + "-" +
                    member?.birthDate?.month.toString().format(2) + "-" + member?.birthDate?.day.toString().format(2)

            binding.viewModel = basicDetailsViewModelNew
            binding.viewModel?.gender?.postValue(member?.gender)
            if (member?.birthDate != null) {
                val c = Calendar.getInstance();
                c.set(member?.birthDate!!.year, member?.birthDate!!.month - 1, member?.birthDate!!.day)
                val format = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                binding.viewModel?.birthDate?.postValue(format.format(c.time))
            }
        } else {
            binding.viewModel = basicDetailsViewModelNew

            if(memberRequest.screeningId.isNotBlank()) {
                binding.editTextCode.setMaskedText(memberRequest.screeningId.removePrefix("P").replace("-",""))
                setDateFromQR()
            }
            else {
                val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY)
                binding.editTextCode.requestFocus()

                binding.memberRequest?.gender = Gender.MALE.gender.toString()
                binding.viewModel?.gender?.postValue("male")
            }

            basicDetailsViewModelNew.screeningIdCheck?.observe(this, Observer { householdId ->
                if (householdId?.status == Status.SUCCESS) {
                    screeningIDValid = false
                    binding.editTextCode.setMaskedText("")
                    val codeCheckDialogFragment = CodeCheckDialogFragment()
                    codeCheckDialogFragment.show(fragmentManager!!)
                } else if (householdId?.status == Status.ERROR) {
                    screeningIDValid = true
                    memberRequest.screeningId = binding.editTextCode.text.toString()
                }
                validateNextButton()
            })

        }

        binding.executePendingBindings()
        binding.nextButton.singleClick {
            val gender = memberRequest.gender.toLowerCase()
            val newStr = gender.toLowerCase()
            val memberId: String? = if (member != null) {
                if (isNetworkAvailable()) member?.memberId!! else member?.uuid!!
            } else {
                null
            }

            val householdIdX: String? = if (member != null) {
                householdId
            } else {
                null
            }
            participantMeta = ParticipantMeta(
                meta = meta!!,
                body = ParticipantX(
                    consentObtained = true,
                    isEligible = true,
                    firstName = "NA",//memberRequest.firstName,
                    lastName = "NA",//memberRequest.screeningId.replace("-",""),//memberRequest.lastName,
                    preferredName = memberRequest.nickName,
                    gender = newStr,
                    hoursFasted = hoursFasted!!,
                    enumerationId = householdIdX,
                    memberId = memberId,
                    idType = "NID",
                    videoWatched = false,
                    alternateContactsDetails = ParticipantAlternateContactsDetails(
                        name = memberRequest.alternateContactsDetails.name,
                        relationship = selectedRelationShip.toString(),
                        address = memberRequest.alternateContactsDetails.address,
                        email = if (memberRequest.alternateContactsDetails.email.isEmpty()) {
                            null
                        } else {
                            memberRequest.alternateContactsDetails.email
                        },
                        phone_preferred = memberRequest.alternateContactsDetails.phonePreferred,
                        phone_alternate = if (memberRequest.alternateContactsDetails.phoneAlternate.isEmpty()) {
                            null
                        } else memberRequest.alternateContactsDetails.phoneAlternate
                    ),
                    age = ParticipantAge(
                        dob = memberRequest.age.dob,
                        ageInYears = memberRequest.age.ageInYears,
                        dobComputed = true
                    ),
                    address = ParticipantAddress(
                        street = memberRequest.address.street,
                        country = memberRequest.address.country,
                        locality = memberRequest.address.locality,
                        postcode = memberRequest.address.postcode
                    ),
                    contactDetails = ParticipantContactDetails(
                        phoneNumberAlternate = if (memberRequest.contactDetails.phoneNumberAlternate.isEmpty()) null else memberRequest.contactDetails.phoneNumberAlternate,
                        phoneNumberPreferred = memberRequest.contactDetails.phoneNumberPreferred,
                        email = if (memberRequest.contactDetails.email.isEmpty()) {
                            null
                        } else memberRequest.contactDetails.email
                    ),
                    comment = null
                )
            )
            participantMeta.body.screeningId = memberRequest.screeningId
            participantMeta.phoneCountryCode = userConfig?.mobileCode
            participantMeta.countryCode = userConfig?.countryCode
            //  Timber.d("par", participantMeta.toString())
            findNavController().navigate(
                R.id.action_BasicDetailFragmentNew_to_reviewFragmentNew,
                bundleOf("participantMeta" to participantMeta, "concentPhotoPath" to concentPhoto)

            )
        }

        val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            basicDetailsViewModelNew.birthYear = year
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear+1)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val birthDate: Date = Date(cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH))
            basicDetailsViewModelNew.birthDate.postValue(sdf.format(cal.time))
            basicDetailsViewModelNew.birthDateVal.postValue(birthDate)
            binding.member?.birthDate = birthDate
            binding.memberRequest?.age?.dob = view.toSimpleDateString(cal.time)

            val years = UserConfig.getAge(
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            )  //Calendar.getInstance().get(Calendar.YEAR) - year

            basicDetailsViewModelNew.age.value = years

            binding.memberRequest?.age?.ageInYears = years

            binding.textViewYears.text = getString(R.string.string_years)
            binding.executePendingBindings()
        }

        binding.birthDate.singleClick {
            var datepicker = DatePickerDialog(
                activity!!, R.style.datepicker, dateSetListener,
                1998,
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            )
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.YEAR, -80)
            datepicker.datePicker.minDate = calendar.timeInMillis
            datepicker.show()
        }

        basicDetailsViewModelNew.setUser("user")
        basicDetailsViewModelNew.user?.observe(this, Observer { userData ->
            if (userData?.data != null) {

                user = userData.data
                val countryCode = user?.team?.country
                userConfig = UserConfig.getUserConfig(countryCode)

                binding.contactNumberPrimaryCodeEditText.setText(userConfig?.mobileCode + " - ")
                binding.contactPersonContactNumberPrimaryCodeEditText.setText(userConfig?.mobileCode + " - ")
                binding.contactNumberPrimaryEditText.filters =
                        arrayOf<InputFilter>(InputFilter.LengthFilter(userConfig?.mobileMaxLength!!))

                memberRequest.address.country = user?.team?.country!!

            }
        })

        onTextChanges(binding.editTextCode)
        onTextChanges(binding.birthDate)
        validateNextButton()

    }

    private fun onTextChanges(editText: EditText) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(editText == binding.birthDate) {
                    validateNextButton()
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(editText == binding.editTextCode) {
                    screeningIDValid = false
                    binding.textLayoutCode.error = ""
                    validateScreeningId()
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                return navController().popBackStack()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun validateNextButton() {

        if (screeningIDValid
            && !binding.memberRequest?.gender.isNullOrBlank()
            && !binding.memberRequest?.age?.ageInYears.isNullOrBlank()
            && validateDOB()) {
            binding.nextButton.setTextColor(Color.parseColor("#0A1D53"))
            binding.nextButton.setDrawableRightColor("#0A1D53")
            binding.nextButton.isEnabled = true
        } else {
            binding.nextButton.setTextColor(Color.parseColor("#AED6F1"));
            binding.nextButton.setDrawableRightColor("#AED6F1")
            binding.nextButton.isEnabled = false
        }

    }
    private fun validateScreeningId() {
        val id = binding.editTextCode.text.toString()
        val checkSum = validateChecksum(id, Constants.TYPE_PARTICIPANT)
        if (!checkSum.error) {
            basicDetailsViewModelNew.setScreeningId(id)
        } else {
            binding.textLayoutCode.error = getString(R.string.invalid_code)//checkSum.message
            binding.nextButton.setTextColor(Color.parseColor("#AED6F1"));
            binding.nextButton.setDrawableRightColor("#AED6F1")
            binding.nextButton.isEnabled = false
        }
    }
    private fun validateDOB() : Boolean
    {
         if(binding.memberRequest?.age?.ageInYears != null && binding.memberRequest?.age?.ageInYears != "" && binding.memberRequest?.age?.ageInYears!!.toInt() > 17)
         {
             binding.inputLayoutBirthDate.error = null
             return true
         }
        else
         {
             binding.inputLayoutBirthDate.error = getString(R.string.error_age)
             return false
         }
    }
    private fun validatePrimaryMobile(): Boolean {

        if (!binding.contactNumberPrimaryEditText.text.isNullOrEmpty()) {
            if (UserConfig.isValidPhoneNumber(binding.contactNumberPrimaryEditText.text.toString(), userConfig!!)) {
                binding.contactNumberPrimaryTextLayout.error = null
                return true
            } else {
                binding.contactNumberPrimaryTextLayout.error = getString(R.string.app_error_valid_phone)
                return false
            }

        } else {
            return false
        }
    }

    private fun validateSecondaryMobile(): Boolean {

        if (!binding.contactNumberSecondryEditText.text.isNullOrEmpty()) {
            if (UserConfig.isValidPhoneNumber(binding.contactNumberSecondryEditText.text.toString(), userConfig!!)) {
                binding.contactNumberSecondryTextLayout.error = null
                // binding.contactNumberSecondryTextLayout.clearFocus()
                return true
            } else {

                //   binding.contactNumberSecondryTextLayout.requestFocus();
                binding.contactNumberSecondryTextLayout.error = getString(R.string.app_error_valid_phone)
                return false
            }

        } else {
            return true // bcz optional
        }
    }

    private fun validateContactSecondaryMobile(): Boolean {

        if (!binding.contactPersonContactNumberSecondryEditText.text.isNullOrEmpty()) {
            if (UserConfig.isValidPhoneNumber(
                    binding.contactPersonContactNumberSecondryEditText.text.toString(),
                    userConfig!!
                )
            ) {
                binding.contactPersonContactNumberSecondryTextLayout.error = null
                // binding.contactPersonContactNumberSecondryTextLayout.clearFocus()
                //binding.contactPersonEmailEditText.requestFocus()

                return true
            } else {

                //binding.contactPersonContactNumberSecondryTextLayout.requestFocus();
                binding.contactPersonContactNumberSecondryTextLayout.error = getString(R.string.app_error_valid_phone)
                return false
            }

        } else {
            return true // bcz optional
        }
    }

    private fun validateContactPrimaryMobile(): Boolean {

        if (!binding.contactPersonContactNumberPrimaryEditText.text.isNullOrEmpty()) {
            if (UserConfig.isValidPhoneNumber(
                    binding.contactPersonContactNumberPrimaryEditText.text.toString(),
                    userConfig!!
                )
            ) {
                binding.contactPersonContactNumberPrimaryTextLayout.error = null
                // binding.contactPersonContactNumberPrimaryTextLayout.clearFocus()
                // binding.contactPersonContactNumberSecondryEditText.requestFocus()
                return true
            } else {

                // binding.contactPersonContactNumberPrimaryTextLayout.requestFocus();
                binding.contactPersonContactNumberPrimaryTextLayout.error = getString(R.string.app_error_valid_phone)
                return false
            }

        } else {
            return false
        }
    }

    private fun validatePrimaryEmail(): Boolean {
        if (!binding.emailEditText.text.isNullOrEmpty()) {
            if (UserConfig.isValidEMail(memberRequest.contactDetails.email)) {
                binding.emailTextLayout.error = ""
                //binding.emailTextLayout.clearFocus()
                return true
            } else {
                binding.emailTextLayout.error = getString(R.string.app_error_valid_email)
                // binding.emailTextLayout.requestFocus()
                return false
            }
        } else {
            return true // bcz optional
        }
    }

    private fun validateContactPersonEmail(): Boolean {
        if (!binding.contactPersonEmailEditText.text.isNullOrEmpty()) {
            if (UserConfig.isValidEMail(memberRequest.alternateContactsDetails.email)) {
                binding.contactPersonEmailTextLayout.error = ""
                //binding.contactPersonEmailTextLayout.clearFocus()
                // binding.nextButton.requestFocus()
                return true
            } else {
                binding.contactPersonEmailTextLayout.error = getString(R.string.app_error_valid_email)
                // binding.contactPersonEmailTextLayout.requestFocus()
                return false
            }
        } else {
            return true // bcz optional
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = activity?.getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    // to set the 24 hours time ------------------------------ 7.2.2020 --------- Nuwan ----------

    private fun convertTimeTo24Hours(): String
    {
        val now: Calendar = Calendar.getInstance()
        val inputFormat: DateFormat = SimpleDateFormat("MMM DD, yyyy HH:mm:ss")
        val outputformat: DateFormat = SimpleDateFormat("HH:mm")
        val date: java.util.Date
        val output: String
        try{
            date= inputFormat.parse(now.time.toLocaleString())
            output = outputformat.format(date)
            return output
        }catch(p: ParseException){
            return ""
        }
    }

    private fun getDate(): String
    {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm")
        val outputformat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        val date: java.util.Date
        val output: String
        try{
            date= inputFormat.parse(binding.root.getLocalTimeString())
            output = outputformat.format(date)

            return output
        }catch(p: ParseException){
            return ""
        }
    }

    private fun setDateFromQR() {

        val dataArr = memberRequest.age.dob.split("-").toTypedArray()
        val year = dataArr[0].toInt()
        val monthOfYear = dataArr[1].toInt()
        val dayOfMonth = dataArr[2].toInt()

        basicDetailsViewModelNew.birthYear = year
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, monthOfYear)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        val birthDate: Date = Date(cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH))
        basicDetailsViewModelNew.birthDate.postValue(sdf.format(cal.time))
        basicDetailsViewModelNew.birthDateVal.postValue(birthDate)
        binding.member?.birthDate = birthDate

        val years = UserConfig.getAge(
            cal.get(Calendar.YEAR),
            cal.get(Calendar.MONTH),
            cal.get(Calendar.DAY_OF_MONTH)
        )  //Calendar.getInstance().get(Calendar.YEAR) - year

        basicDetailsViewModelNew.age.value = years

        binding.memberRequest?.age?.ageInYears = years

        binding.textViewYears.text = getString(R.string.string_years)
        binding.executePendingBindings()
        validateNextButton()
    }

    // -------------------------------------------------------------------------------------------


    /**
     * Created to be able to override in tests
     */
    fun navController() = findNavController()
}





