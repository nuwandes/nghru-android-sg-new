package org.singapore.ghru.ui.fundoscopy.questions

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class FundoscopyQuestionsViewModel : ViewModel() {

    val hadSurgery = MutableLiveData<Boolean>()
    val haveSymptoms = MutableLiveData<Boolean>()

    fun setHadSurgery(item: Boolean) {
        hadSurgery.value = item
    }

    fun setHaveSymptoms(item: Boolean) {
        haveSymptoms.value = item
    }

}
