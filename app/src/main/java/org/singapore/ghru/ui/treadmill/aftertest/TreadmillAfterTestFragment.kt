package org.singapore.ghru.ui.treadmill.aftertest

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.crashlytics.android.Crashlytics
import com.google.gson.GsonBuilder

import org.singapore.ghru.R
import org.singapore.ghru.binding.FragmentDataBindingComponent
import org.singapore.ghru.databinding.TreadmillAfterTestFragmentBinding
import org.singapore.ghru.di.Injectable
import org.singapore.ghru.ui.treadmill.completed.CompletedDialogFragment
import org.singapore.ghru.util.autoCleared
import org.singapore.ghru.util.singleClick
import org.singapore.ghru.vo.Status
import org.singapore.ghru.vo.request.ParticipantRequest
import org.singapore.ghru.vo.request.TreadmillBody
import javax.inject.Inject

class TreadmillAfterTestFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var binding by autoCleared<TreadmillAfterTestFragmentBinding>()
    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    @Inject
    lateinit var viewModel: TreadmillAfterTestViewModel

    private var participant: ParticipantRequest? = null
    private var treadmillBody: TreadmillBody? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            participant = arguments?.getParcelable<ParticipantRequest>("participant")!!
            treadmillBody = arguments?.getParcelable<TreadmillBody>("treadmillBody")!!
        } catch (e: KotlinNullPointerException) {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil.inflate<TreadmillAfterTestFragmentBinding>(
            inflater,
            R.layout.treadmill_after_test_fragment,
            container,
            false
        )
        binding = dataBinding

        setHasOptionsMenu(true)
        val appCompatActivity = requireActivity() as AppCompatActivity
        appCompatActivity.setSupportActionBar(binding.detailToolbar)
        appCompatActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.setLifecycleOwner(this)
        binding.participant = participant
        binding.viewModel = viewModel

        binding.previousButton.singleClick {
            navController().popBackStack()
        }

        viewModel.treadmillComplete?.observe(this, Observer { participant ->

            if (participant?.status == Status.SUCCESS) {
                val completedDialogFragment = CompletedDialogFragment()
                completedDialogFragment.arguments = bundleOf("is_cancel" to false)
                completedDialogFragment.show(fragmentManager!!)
            } else if (participant?.status == Status.ERROR) {
                Crashlytics.setString("participant", participant.toString())
                Crashlytics.logException(Exception("treadmillComplete " + participant.message.toString()))
                binding.executePendingBindings()
            }
        })
        binding.nextButton.singleClick {
            if (validateNextButton()) {
                val gson = GsonBuilder().setPrettyPrinting().create()
                treadmillBody?.after_test = getAfterTestQuestions()
                viewModel.setParticipantComplete(
                    participant!!, isNetworkAvailable(),
                    gson.toJson(treadmillBody)
                )
            }
        }

        viewModel.rating?.observe(
            this,
            Observer { rating ->
                validateRating(rating)
                validateNextButton()
            })

        binding.radioGroupChest.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.noChest) {
                binding.radioGroupChestValue = false
                viewModel.setChestPain(false)

            } else {
                binding.radioGroupChestValue = false
                viewModel.setChestPain(true)

            }
            binding.executePendingBindings()
        }
        binding.radioGroupBreath.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.noBreath) {
                binding.radioGroupBreathlessValue = false
                viewModel.setBreathless(false)

            } else {
                binding.radioGroupBreathlessValue = false
                viewModel.setBreathless(true)

            }
            binding.executePendingBindings()
        }
        binding.radioGroupBar.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.noBar) {
                binding.radioGroupBarValue = false
                viewModel.setFrontBar(false)

            } else {
                binding.radioGroupBarValue = false
                viewModel.setFrontBar(true)

            }
            binding.executePendingBindings()
        }
    }

    private fun validateRating(rating: String) {
        try {
            val ratingVal: Double = rating.toDouble()
            if (ratingVal >= 6 && ratingVal <= 20) {
                binding.ratingInputLayout.error = null
                viewModel.isValidRating = true

            } else {
                viewModel.isValidRating = false
                binding.ratingInputLayout.error = getString(R.string.error_not_in_range)
            }

        } catch (e: Exception) {
            viewModel.isValidRating = false
            binding.ratingInputLayout.error = getString(R.string.error_invalid_input)
        }
    }

    private fun validateNextButton(): Boolean {
        if(viewModel.rating?.value.isNullOrBlank() || !viewModel.isValidRating) {
            return false
        }
        if(viewModel.chestPain.value == null) {
            binding.radioGroupChestValue = true
            binding.executePendingBindings()
            return false
        }
        if(viewModel.breathless.value == null) {
            binding.radioGroupBreathlessValue = true
            binding.executePendingBindings()
            return false
        }
        if(viewModel.frontBar.value == null) {
            binding.radioGroupBarValue = true
            binding.executePendingBindings()
            return false
        }

        return true
    }

    private fun getAfterTestQuestions(): MutableList<Map<String, String>> {
        var afterTest = mutableListOf<Map<String, String>>()

        val rating = viewModel.rating.value
        val chestPain = viewModel.chestPain.value
        val breathless = viewModel.breathless.value
        val frontBar = viewModel.frontBar.value

        var ratingMap = mutableMapOf<String, String>()
        ratingMap["question"] = getString(R.string.treadmill_rating_question)
        ratingMap["answer"] = rating!!

        afterTest.add(ratingMap)

        var chestPainMap = mutableMapOf<String, String>()
        chestPainMap["question"] = getString(R.string.treadmill_after_chest_pain_question)
        chestPainMap["answer"] = if (chestPain!!) "yes" else "no"

        afterTest.add(chestPainMap)

        var breathMap = mutableMapOf<String, String>()
        breathMap["question"] = getString(R.string.treadmill_after_breathless_question)
        breathMap["answer"] = if (breathless!!) "yes" else "no"

        afterTest.add(breathMap)

        var barMap = mutableMapOf<String, String>()
        barMap["question"] = getString(R.string.treadmill_front_bar_question)
        barMap["answer"] = if (frontBar!!) "yes" else "no"

        afterTest.add(barMap)

        return afterTest
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = activity?.getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    /**
     * Created to be able to override in tests
     */
    fun navController() = findNavController()

}
