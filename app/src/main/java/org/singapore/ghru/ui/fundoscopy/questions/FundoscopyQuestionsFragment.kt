package org.singapore.ghru.ui.fundoscopy.questions

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController

import org.singapore.ghru.R
import org.singapore.ghru.binding.FragmentDataBindingComponent
import org.singapore.ghru.databinding.DisplayBarcodeBinding
import org.singapore.ghru.databinding.FundoscopyQuestionsFragmentBinding
import org.singapore.ghru.ui.ecg.questions.ECGSkipFragment
import org.singapore.ghru.ui.ecg.questions.TYPE_FUNDO
import org.singapore.ghru.util.autoCleared
import org.singapore.ghru.util.singleClick
import org.singapore.ghru.vo.request.ParticipantRequest
import javax.inject.Inject

class FundoscopyQuestionsFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var binding by autoCleared<FundoscopyQuestionsFragmentBinding>()
    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    @Inject
    lateinit var fundosQuestionnaireViewModel: FundoscopyQuestionsViewModel

    private var participant: ParticipantRequest? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            participant = arguments?.getParcelable<ParticipantRequest>("participant")!!
        } catch (e: KotlinNullPointerException) {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil.inflate<FundoscopyQuestionsFragmentBinding>(
            inflater,
            R.layout.fundoscopy_questions_fragment,
            container,
            false
        )
        binding = dataBinding

        setHasOptionsMenu(true)
        val appCompatActivity = requireActivity() as AppCompatActivity
        appCompatActivity.setSupportActionBar(binding.detailToolbar)
        appCompatActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fundosQuestionnaireViewModel = activity?.run {
            ViewModelProviders.of(this).get(FundoscopyQuestionsViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.setLifecycleOwner(this)
        binding.participant = participant

        binding.previousButton.singleClick {
            navController().popBackStack()
        }

        binding.nextButton.singleClick {
            if (validateNextButton()) {
                val bundle = Bundle()
                bundle.putParcelable("participant", participant)
                navController().navigate(R.id.action_questionnaireFragment_to_guideMainFragment, bundle)
            }
        }

        binding.radioGroupSurgery.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.no) {
                binding.radioGroupSurgeryValue = false
                fundosQuestionnaireViewModel.setHadSurgery(false)

            } else {
                binding.radioGroupSurgeryValue = false
                fundosQuestionnaireViewModel.setHadSurgery(true)

            }
            binding.executePendingBindings()
        }
        binding.radioGroupSymptoms.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.symptomsNo) {
                binding.radioGroupSymptomsValue = false
                fundosQuestionnaireViewModel.setHaveSymptoms(false)

            } else {
                binding.radioGroupSymptomsValue = false
                fundosQuestionnaireViewModel.setHaveSymptoms(true)

            }
            binding.executePendingBindings()
        }

    }

    private fun validateNextButton(): Boolean {
        if(fundosQuestionnaireViewModel.hadSurgery.value == null) {
            binding.radioGroupSurgeryValue = true
            binding.executePendingBindings()
            return false
        }
        else if(fundosQuestionnaireViewModel.haveSymptoms.value == null) {
            binding.radioGroupSymptomsValue = true
            binding.executePendingBindings()
            return false
        }

        if(fundosQuestionnaireViewModel.hadSurgery.value ==  true || fundosQuestionnaireViewModel.haveSymptoms.value ==  true) {
            val skipDialogFragment = ECGSkipFragment()
            skipDialogFragment.arguments = bundleOf("participant" to participant,
                "contraindications" to getContraindications(),
                "type" to TYPE_FUNDO)
            skipDialogFragment.show(fragmentManager!!)

            return false
        }

        return true
    }

    private fun getContraindications(): MutableList<Map<String, String>> {
        var contraindications = mutableListOf<Map<String, String>>()

        val hadSurgery = fundosQuestionnaireViewModel.hadSurgery.value
        val haveSymptoms = fundosQuestionnaireViewModel.haveSymptoms.value

        var symptomsMap = mutableMapOf<String, String>()
        symptomsMap["question"] = getString(R.string.funduscopy_symptoms_question)
        symptomsMap["answer"] = if (haveSymptoms!!) "yes" else "no"

        contraindications.add(symptomsMap)

        var surgeryMap = mutableMapOf<String, String>()
        surgeryMap["question"] = getString(R.string.funduscopy_surgery_question)
        surgeryMap["answer"] = if (hadSurgery!!) "yes" else "no"

        contraindications.add(surgeryMap)

        return contraindications
    }

    /**
     * Created to be able to override in tests
     */
    fun navController() = findNavController()

}
