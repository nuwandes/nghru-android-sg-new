package org.singapore.ghru.ui.visualacuity.home


import android.content.Context
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.birbit.android.jobqueue.JobManager
import com.crashlytics.android.Crashlytics
import io.reactivex.disposables.CompositeDisposable
import org.singapore.ghru.R
import org.singapore.ghru.binding.FragmentDataBindingComponent
import org.singapore.ghru.databinding.VisualAcuityHomeFragmentBinding
import org.singapore.ghru.di.Injectable
import org.singapore.ghru.event.*
import org.singapore.ghru.ui.heightweight.completed.CompletedDialogFragment
import org.singapore.ghru.ui.visualacuity.reason.ReasonDialogFragment
import org.singapore.ghru.ui.visualacuity.contraindication.VisualAcuityQuestionnaireViewModel
import org.singapore.ghru.util.*
import org.singapore.ghru.vo.*
import org.singapore.ghru.vo.request.*
import timber.log.Timber
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.Date
import javax.inject.Inject


class VisualAcuityHomeFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var binding: VisualAcuityHomeFragmentBinding


    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    @Inject
    lateinit var viewModel: VisualAcuityHomeViewModel

    private var sampleRequest: SampleRequest? = null

    private val disposables = CompositeDisposable()

    @Inject
    lateinit var jobManager: JobManager

//    private var hipData: HipWaistData? = null
//    private var waistData: HipWaistData? = null
//    private var hipWaistData: HipWaistTests? = null

    private var leftEyeData: VisualAcuityData? = null
    private var rightEyeData: VisualAcuityData? = null
    private var visualAcuityData: VisualAcuityTests? = null

    private var participantRequest: ParticipantRequest? = null
    private var visualAidValue: String? = null
    private var imageExported: Boolean? = false

    var user: User? = null
    var meta: Meta? = null

    private lateinit var questionnaireViewModel: VisualAcuityQuestionnaireViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            participantRequest = arguments?.getParcelable<ParticipantRequest>("ParticipantRequest")!!
            visualAidValue = arguments?.getString("visualAid")!!
//            imageExported = arguments?.getBoolean("imageExported")!!

        } catch (e: KotlinNullPointerException) {
            //Crashlytics.logException(e)
        }

        disposables.add(
            LeftEyeRecordTestRxBus.getInstance().toObservable()
                .subscribe({ result ->

                    Timber.d(result.toString())
                    leftEyeData = result

                }, { error ->
                    print(error)
                    error.printStackTrace()
                })
        )

        disposables.add(
            RightEyeRecordTestRxBus.getInstance().toObservable()
                .subscribe({ result ->

                    Timber.d(result.toString())
                    rightEyeData = result

                }, { error ->
                    print(error)
                    error.printStackTrace()
                })
        )
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil.inflate<VisualAcuityHomeFragmentBinding>(
            inflater,
            R.layout.visual_acuity_home_fragment,
            container,
            false
        )
        binding = dataBinding
        setHasOptionsMenu(true)
        val appCompatActivity = requireActivity() as AppCompatActivity
        appCompatActivity.setSupportActionBar(binding.detailToolbar)
        appCompatActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.root.hideKeyboard()
        return dataBinding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.setLifecycleOwner(this)
        binding.viewModel = viewModel
        binding.sample = sampleRequest
        binding.participant = participantRequest

        viewModel.setUser("user")
        viewModel.user?.observe(this, Observer { userData ->
            if (userData?.data != null) {
                // setupNavigationDrawer(userData.data)
                user = userData.data

                val sTime: String = convertTimeTo24Hours()
                val sDate: String = getDate()
                val sDateTime:String = sDate + " " + sTime

                meta = Meta(collectedBy = user?.id, startTime = sDateTime)
                //meta?.registeredBy = user?.id
            }

        })

        if (leftEyeData != null)
        {
                binding.hipCompleteView.visibility = View.VISIBLE
                binding.linearLayoutHip.background = resources.getDrawable(R.drawable.ic_process_complete_bg, null)

        }

        if (rightEyeData != null)
        {
            binding.waistCompleteView.visibility = View.VISIBLE
            binding.linearLayoutWaist.background =
                resources.getDrawable(R.drawable.ic_process_complete_bg, null)
        }

        Log.d("VISUAL_ACUITY_HOME","DATA:" + visualAidValue.toString())
        binding.errorView.collapse()

//        viewModel.sampleMangementPocess?.observe(this, Observer { sampleMangementPocess ->
//            // Timber.d(sampleMangementPocess.toString())
//            if (sampleMangementPocess?.status == Status.SUCCESS) {
//                activity!!.finish()
//            } else if (sampleMangementPocess?.status == Status.ERROR) {
//                binding.progressBar.visibility = View.GONE
//                binding.buttonSubmit.visibility = View.VISIBLE
//                //Crashlytics.logException(Exception(sampleMangementPocess.message?.message))
//                //var error = accessToken.dat
//            }
//        })

        viewModel.visualMeasurementMetaOffline?.observe(this, Observer { sampleMangementPocess ->

            if(sampleMangementPocess?.status == Status.LOADING){
                binding.progressBar.visibility = View.VISIBLE
                binding.buttonSubmit.visibility = View.GONE
            }else{
                binding.progressBar.visibility = View.GONE
                binding.buttonSubmit.visibility = View.VISIBLE
            }

            if (sampleMangementPocess?.status == Status.SUCCESS) {
                val completedDialogFragment = CompletedDialogFragment()
                completedDialogFragment.arguments = bundleOf("is_cancel" to false)
                completedDialogFragment.show(fragmentManager!!)
            } else if(sampleMangementPocess?.status == Status.ERROR){
                Crashlytics.setString(
                    "VisualAcuityMeasurementMeta",
                    VisualAcuityRequest(meta = meta, body = visualAcuityData).toString()
                )
                Crashlytics.setString("participant", participantRequest.toString())
                Crashlytics.logException(Exception("BodyMeasurementMeta " + sampleMangementPocess.message.toString()))
            }
        })




        binding.buttonCancel.singleClick {
            val reasonDialogFragment = ReasonDialogFragment()
            reasonDialogFragment.arguments = bundleOf("participant" to participantRequest)
            reasonDialogFragment.show(fragmentManager!!)
        }

        binding.buttonSubmit.singleClick {

            if (leftEyeData != null && rightEyeData != null && validateNextButton()) {
                visualAcuityData = VisualAcuityTests(
                    left_eye = leftEyeData,
                    right_eye = rightEyeData,
                    comment = "",
                    visual_aid = visualAidValue,
                    images_exported = imageExported!!

                )
                visualAcuityData!!.contraindications = getContraindications()

                val endTime: String = convertTimeTo24Hours()
                val endDate: String = getDate()
                val endDateTime:String = endDate + " " + endTime

                meta?.endTime = endDateTime

                val visualAcuityRequest = VisualAcuityRequest(meta = meta, body = visualAcuityData)
                visualAcuityRequest.screeningId = participantRequest?.screeningId!!
                if(isNetworkAvailable()){
                    visualAcuityRequest.syncPending =false
                }else{
                    visualAcuityRequest.syncPending =true

                }

                viewModel.setVisualMeasurementMeta(visualAcuityRequest)

                binding.progressBar.visibility = View.VISIBLE
                binding.buttonSubmit.visibility = View.GONE

            } else {
                binding.errorView.expand()
                binding.sampleValidationError = true
                if (leftEyeData == null) {
                    updateProcessErrorUI(binding.hipTextView)
                }

                if (rightEyeData == null) {
                    updateProcessErrorUI(binding.waistTextView)

                }
                binding.executePendingBindings()
            }
        }

        binding.linearLayoutHip.singleClick {

            binding.sampleValidationError = false
            updateProcessValidUI(binding.hipTextView)
            updateProcessValidUI(binding.waistTextView)

            val bundle = Bundle()
            bundle.putParcelable("ParticipantRequest", participantRequest)
            navController().navigate(R.id.action_VisualAcuityHomeFragment_to_LeftEyeFragment, bundle)
        }



        binding.linearLayoutWaist.singleClick {
            binding.sampleValidationError = false
            updateProcessValidUI(binding.hipTextView)
            updateProcessValidUI(binding.waistTextView)

            val bundle = Bundle()
            bundle.putParcelable("ParticipantRequest", participantRequest)
            navController().navigate(R.id.action_VisualAcuityHomeFragment_to_RightEyeFragment, bundle)
        }

        binding.radioGroupImageExport.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.noImageExport) {
                binding.radioGroupImageExportValue = false
                viewModel.setImageExport(false)
                imageExported = false

            } else {
                binding.radioGroupImageExportValue = false
                viewModel.setImageExport(true)
                imageExported = true
            }
            binding.executePendingBindings()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        questionnaireViewModel = activity?.run {
            ViewModelProviders.of(this).get(VisualAcuityQuestionnaireViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    private fun getContraindications(): MutableList<Map<String, String>> {
        val contraindications: MutableList<Map<String, String>> = mutableListOf()

        val haveEyeOccluded = questionnaireViewModel.haveEyeOccluded.value

        val colostomyMap = mutableMapOf<String, String>()
        colostomyMap["question"] = "With one eye occluded, able to stare for prolonged period?"
        colostomyMap["answer"] = if (haveEyeOccluded!!) "yes" else "no"
        contraindications.add(colostomyMap)

        return contraindications
    }

    private fun updateProcessErrorUI(view: TextView) {
        view.setTextColor(Color.parseColor("#FF5E45"))
        view.setDrawbleLeftColor("#FF5E45")
    }

    private fun updateProcessValidUI(view: TextView) {
        view.setTextColor(Color.parseColor("#00548F"))
        view.setDrawbleLeftColor("#00548F")
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = activity?.getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }


    override fun onResume() {
        super.onResume()
        BusProvider.getInstance().register(this)
    }

    override fun onPause() {
        super.onPause()
        BusProvider.getInstance().unregister(this)
    }

    private fun convertTimeTo24Hours(): String
    {
        val now: Calendar = Calendar.getInstance()
        val inputFormat: DateFormat = SimpleDateFormat("MMM DD, yyyy HH:mm:ss")
        val outputformat: DateFormat = SimpleDateFormat("HH:mm")
        val date: Date
        val output: String
        try{
            date= inputFormat.parse(now.time.toLocaleString())
            output = outputformat.format(date)
            return output
        }catch(p: ParseException){
            return ""
        }
    }

    private fun getDate(): String
    {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm")
        val outputformat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        val date: Date
        val output: String
        try{
            date= inputFormat.parse(binding.root.getLocalTimeString())
            output = outputformat.format(date)

            return output
        }catch(p: ParseException){
            return ""
        }
    }

    private fun validateNextButton(): Boolean {
        if(viewModel.haveImageExport.value == null) {
            binding.radioGroupImageExportValue = true
            binding.executePendingBindings()
            return false
        }
        else
        {
            return true
        }
    }

    /**
     * Created to be able to override in tests
     */
    fun navController() = findNavController()


}
