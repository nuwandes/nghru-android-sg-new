package org.singapore.ghru.ui.ecg.trace


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import org.singapore.ghru.R
import org.singapore.ghru.binding.FragmentDataBindingComponent
import org.singapore.ghru.databinding.TraceFragmentBinding
import org.singapore.ghru.di.Injectable
import org.singapore.ghru.ui.ecg.questions.ECGQuestionnaireViewModel
import org.singapore.ghru.ui.ecg.trace.complete.CompleteDialogFragment
import org.singapore.ghru.ui.ecg.trace.reason.ReasonDialogFragment
import org.singapore.ghru.util.autoCleared
import org.singapore.ghru.util.singleClick
import org.singapore.ghru.vo.Measurements
import org.singapore.ghru.vo.StationDeviceData
import org.singapore.ghru.vo.Status
import org.singapore.ghru.vo.request.ParticipantRequest
import javax.inject.Inject

class TraceFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var binding by autoCleared<TraceFragmentBinding>()
    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)
    @Inject
    lateinit var verifyIDViewModel: TraceViewModel

    private var participant: ParticipantRequest? = null

    private var deviceListName: MutableList<String> = arrayListOf()
    private var deviceListObject: List<StationDeviceData> = arrayListOf()
    private var selectedDeviceID: String? = null

    private lateinit var questionnaireViewModel: ECGQuestionnaireViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            participant = arguments?.getParcelable<ParticipantRequest>("participant")!!
        } catch (e: KotlinNullPointerException) {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil.inflate<TraceFragmentBinding>(
            inflater,
            R.layout.trace_fragment,
            container,
            false
        )
        binding = dataBinding

        setHasOptionsMenu(true)
        val appCompatActivity = requireActivity() as AppCompatActivity
        appCompatActivity.setSupportActionBar(binding.detailToolbar)
        appCompatActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        questionnaireViewModel = activity?.run {
            ViewModelProviders.of(this).get(ECGQuestionnaireViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.setLifecycleOwner(this)
        binding.participant = participant
        // binding.viewModel = verifyIDViewModel
        binding.buttonCancel.singleClick {
            val reasonDialogFragment = ReasonDialogFragment()
            reasonDialogFragment.arguments = bundleOf(
                "participant" to participant,
                "contraindications" to getContraindications(),
                "skipped" to false)
            reasonDialogFragment.show(fragmentManager!!)
        }


        binding.buttonSubmit.singleClick {

            if(selectedDeviceID==null)
            {
                binding.textViewDeviceError.visibility = View.VISIBLE
            }
            else {
                val completeDialogFragment = CompleteDialogFragment()
//            val bundle = Bundle()
//            bundle.putParcelable("participant", participant)
//            bundle.putString("comment", binding.comment.text.toString())
//            bundle.putString("deviceId",selectedDeviceID)
                completeDialogFragment.arguments = bundleOf(
                    "participant" to participant,
                    "comment" to binding.comment.text.toString(),
                    "deviceId" to selectedDeviceID
                )
                completeDialogFragment.show(fragmentManager!!)
            }
        }

        deviceListName.clear()
        deviceListName.add(getString(R.string.unknown))
        val adapter = ArrayAdapter(context!!, R.layout.basic_spinner_dropdown_item, deviceListName)
        binding.deviceIdSpinner.setAdapter(adapter);

        verifyIDViewModel.setStationName(Measurements.ECG)
        verifyIDViewModel.stationDeviceList?.observe(this, Observer {
            if (it.status.equals(Status.SUCCESS)) {
                deviceListObject = it.data!!

                deviceListObject.iterator().forEach {
                    deviceListName.add(it.device_name!!)
                }
                adapter.notifyDataSetChanged()
            }
        })
        binding.deviceIdSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>, @NonNull selectedItemView: View?,
                position: Int,
                id: Long
            ) {
                if (position == 0) {
                    selectedDeviceID = null
                } else {
                    binding.textViewDeviceError.visibility = View.GONE
                    selectedDeviceID = deviceListObject[position - 1].device_id
                }

            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // your code here
            }

        }
    }

    private fun getContraindications(): MutableList<Map<String, String>> {
        var contraindications = mutableListOf<Map<String, String>>()

        val haveArteriovenous = questionnaireViewModel.haveArteriovenous.value
        val hadSurgery = questionnaireViewModel.hadSurgery.value
        val lymphRemoved = questionnaireViewModel.lymphRemoved.value
        val haveTrauma = questionnaireViewModel.haveTrauma.value
        val haveNeckInjury = questionnaireViewModel.haveNeckInjury.value
        val amputated = questionnaireViewModel.amputated.value

        var arteriovenousMap = mutableMapOf<String, String>()
        arteriovenousMap["question"] = "Arteriovenous Fistula in both arms?"
        arteriovenousMap["answer"] = if (haveArteriovenous!!) "yes" else "no"

        contraindications.add(arteriovenousMap)

        var surgeryMap = mutableMapOf<String, String>()
        surgeryMap["question"] = "Breast surgery (left or right)?"
        surgeryMap["answer"] = hadSurgery!!

        contraindications.add(surgeryMap)

        var lymphRemovedMap = mutableMapOf<String, String>()
        lymphRemovedMap["question"] = "Lymph node(s) removed from armpit?"
        lymphRemovedMap["answer"] = if (lymphRemoved!!) "yes" else "no"

        contraindications.add(lymphRemovedMap)

        var traumaMap = mutableMapOf<String, String>()
        traumaMap["question"] = "Trauma -> swelling (left/ right)?"
        traumaMap["answer"] = haveTrauma!!

        contraindications.add(traumaMap)

        var neckInjuryMap = mutableMapOf<String, String>()
        neckInjuryMap["question"] = "Have had recent neck injury or surgery?"
        neckInjuryMap["answer"] = if (haveNeckInjury!!) "yes" else "no"

        contraindications.add(neckInjuryMap)

        var amputatedMap = mutableMapOf<String, String>()
        amputatedMap["question"] = "Both Arms / Legs Amputated?"
        amputatedMap["answer"] = if (amputated!!) "yes" else "no"

        contraindications.add(amputatedMap)

        return contraindications
    }

    /**
     * Created to be able to override in tests
     */
    fun navController() = findNavController()
}
