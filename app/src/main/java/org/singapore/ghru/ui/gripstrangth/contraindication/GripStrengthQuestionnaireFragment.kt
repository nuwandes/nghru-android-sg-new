package org.singapore.ghru.ui.gripstrangth.contraindication

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController

import org.singapore.ghru.R
import org.singapore.ghru.binding.FragmentDataBindingComponent
import org.singapore.ghru.databinding.GripStrengthContraindicationFragmentBinding
import org.singapore.ghru.databinding.HeightWeightContraindicationFragmentBinding
import org.singapore.ghru.databinding.HipWaistContraindicationFragmentBinding
import org.singapore.ghru.event.BusProvider
import org.singapore.ghru.util.autoCleared
import org.singapore.ghru.util.hideKeyboard
import org.singapore.ghru.util.singleClick
import org.singapore.ghru.vo.request.ParticipantRequest
import javax.inject.Inject

class GripStrengthQuestionnaireFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var binding by autoCleared<GripStrengthContraindicationFragmentBinding>()
    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    @Inject
    lateinit var viewModel: GripStrengthQuestionnaireViewModel

    private var participantRequest: ParticipantRequest? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            participantRequest = arguments?.getParcelable<ParticipantRequest>("ParticipantRequest")!!
        } catch (e: KotlinNullPointerException) {
            //Crashlytics.logException(e)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil.inflate<GripStrengthContraindicationFragmentBinding>(
            inflater,
            R.layout.grip_strength_contraindication_fragment,
            container,
            false
        )
        binding = dataBinding

        //setHasOptionsMenu(true)
        val appCompatActivity = requireActivity() as AppCompatActivity
        appCompatActivity.setSupportActionBar(binding.detailToolbar)
        appCompatActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = activity?.run {
            ViewModelProviders.of(this).get(GripStrengthQuestionnaireViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.participant = participantRequest

        binding.setLifecycleOwner(this)

        binding.nextButton.singleClick {
            if (validateNextButton()) {
                val bundle = Bundle()
                bundle.putParcelable("ParticipantRequest", participantRequest)
                navController().navigate(R.id.action_contraFragment_to_gripStrengthHomeFragment, bundle)
            }
        }

        binding.radioGroupPlaster.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.noPlaster) {
                binding.radioGroupPlasterValue = false
                viewModel.setHavePlaster(false)

            } else {
                binding.radioGroupPlasterValue = false
                viewModel.setHavePlaster(true)

            }
            binding.executePendingBindings()
        }

        binding.radioGroupJoint.setOnCheckedChangeListener { radioGroup, i ->
            if (radioGroup.checkedRadioButtonId == R.id.noJoint) {
                binding.radioGroupJointValue = false
                viewModel.setHaveJoint(false)

            } else {
                binding.radioGroupJointValue = false
                viewModel.setHaveJoint(true)

            }
            binding.executePendingBindings()
        }
    }

    private fun validateNextButton(): Boolean {
        if(viewModel.havePlaster.value == null) {
            binding.radioGroupPlasterValue = true
            binding.executePendingBindings()
            return false
        }

        if(viewModel.haveJoint.value == null) {
            binding.radioGroupJointValue = true
            binding.executePendingBindings()
            return false
        }

        if(viewModel.havePlaster.value ==  true || viewModel.haveJoint.value == true) {
            val skipDialogFragment = GripStrengthSkipFragment()
            skipDialogFragment.arguments = bundleOf("participant" to participantRequest,
                "contraindications" to getContraindications(),
                "type" to TYPE_GRIP_STRENGTH)
            skipDialogFragment.show(fragmentManager!!)

            return false
        }

        return true
    }

    /**
     * Created to be able to override in tests
     */
    fun navController() = findNavController()

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                binding.root.hideKeyboard()
                navController().popBackStack()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun getContraindications(): MutableList<Map<String, String>> {
        val contraindications = mutableListOf<Map<String, String>>()

        val havePlaster = viewModel.havePlaster.value
        val plasterMap = mutableMapOf<String, String>()
        plasterMap["question"] = "Arm in plaster or recent fracture /injury?"
        plasterMap["answer"] = if (havePlaster!!) "yes" else "no"

        contraindications.add(plasterMap)

        val haveJoint = viewModel.haveJoint.value
        val jointMap = mutableMapOf<String, String>()
        jointMap["question"] = "Severe joint condition (wrist and/or fingers, e.g. rheumatoid arthritis?"
        jointMap["answer"] = if (haveJoint!!) "yes" else "no"

        contraindications.add(jointMap)

        return contraindications
    }

    override fun onResume() {
        super.onResume()
        BusProvider.getInstance().register(this)
    }

    override fun onPause() {
        super.onPause()
        BusProvider.getInstance().unregister(this)
    }

}
