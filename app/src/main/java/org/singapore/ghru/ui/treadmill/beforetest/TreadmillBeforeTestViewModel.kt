package org.singapore.ghru.ui.treadmill.beforetest

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TreadmillBeforeTestViewModel : ViewModel() {

    val armPlacement = MutableLiveData<String>()

    fun setArmPlacement(item: String) {
        armPlacement.value = item
    }
}
