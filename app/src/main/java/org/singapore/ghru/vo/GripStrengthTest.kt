package org.singapore.ghru.vo

import android.os.Parcel
import android.os.Parcelable
import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.nuvoair.sdk.launcher.NuvoairLauncherMeasurement
import java.io.Serializable

@Entity(tableName = "grip_strength_request")
data class GripStrengthRequest(
    @Embedded(prefix = "body") @Expose @SerializedName("body") var body: GripStrengthTests?,
    @Embedded(prefix = "meta") @Expose @SerializedName("meta") var meta: Meta?
) :
    Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(GripStrengthTests::class.java.classLoader),
        parcel.readParcelable(Meta::class.java.classLoader)
    ) {
    }


    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
    @ColumnInfo(name = "timestamp")
    var timestamp: Long = System.currentTimeMillis()

    @ColumnInfo(name = "sync_pending")
    var syncPending: Boolean = false

    @ColumnInfo(name = "screening_id")
    lateinit var screeningId: String

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(body, flags)
        parcel.writeParcelable(meta, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GripStrengthRequest> {
        override fun createFromParcel(parcel: Parcel): GripStrengthRequest {
            return GripStrengthRequest(parcel)
        }

        override fun newArray(size: Int): Array<GripStrengthRequest?> {
            return arrayOfNulls(size)
        }
    }

}

data class GripStrengthTests(
    @Embedded(prefix = "right_grip") @Expose @SerializedName("right_grip") var rightGrip: GripStrengthData?,
    @Embedded(prefix = "left_grip") @Expose @SerializedName("left_grip") var leftGrip: GripStrengthData?
): Serializable, Parcelable {

    @Ignore
    @Expose
    @SerializedName("contraindications")
    var contraindications: List<Map<String, String>>? = null

    constructor(parcel: Parcel) : this(
        parcel.readParcelable(GripStrengthData::class.java.classLoader),
        parcel.readParcelable(GripStrengthData::class.java.classLoader)

    ) {
        readContraindications(parcel)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(rightGrip, flags)
        parcel.writeParcelable(leftGrip, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GripStrengthTests> {
        override fun createFromParcel(parcel: Parcel): GripStrengthTests {
            return GripStrengthTests(parcel)
        }

        override fun newArray(size: Int): Array<GripStrengthTests?> {
            return arrayOfNulls(size)
        }

        private fun readContraindications(parcel: Parcel): List<Map<String, String>> {
            val list = mutableListOf<Map<String, String>>()
            parcel.readList(list as List<*>, Map::class.java.classLoader)

            return list
        }
    }

}

data class GripStrengthData(
    @Expose @field:SerializedName("comment") var comment: String?,
    @Expose @field:SerializedName("device_id") var device_id: String?,
    @Expose @field:SerializedName("unit") var unit: String?
): Serializable, Parcelable {

    @Ignore
    @Expose
    @SerializedName("value")
    var value: ArrayList<String>? = null

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(comment)
        parcel.writeString(device_id)
        parcel.writeString(unit)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GripStrengthData> {
        override fun createFromParcel(parcel: Parcel): GripStrengthData {
            return GripStrengthData(parcel)
        }

        override fun newArray(size: Int): Array<GripStrengthData?> {
            return arrayOfNulls(size)
        }
    }
}

data class GripStrengthTest(
    @Expose @field:SerializedName("reading") var reading: Double?
) : Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Double::class.java.classLoader) as? Double
    ) {
    }

    constructor() : this(null)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(reading)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GripStrengthTest> {
        override fun createFromParcel(parcel: Parcel): GripStrengthTest {
            return GripStrengthTest(parcel)
        }

        override fun newArray(size: Int): Array<GripStrengthTest?> {
            return arrayOfNulls(size)
        }
    }
}