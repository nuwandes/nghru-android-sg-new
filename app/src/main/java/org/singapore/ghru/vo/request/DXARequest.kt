package org.singapore.ghru.vo.request

import android.os.Parcel
import android.os.Parcelable
import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.singapore.ghru.vo.Meta
import java.io.Serializable

@Entity(
    tableName = "dxa_request"
)
data class DXARequest(
    @Expose @SerializedName(value = "body")  @ColumnInfo(name = "body") var body: String? = null,
    @Embedded(prefix = "meta") @Expose @field:SerializedName("meta") var meta: Meta?
) : Serializable, Parcelable {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @ColumnInfo(name = "sync_pending")
    var syncPending: Boolean = false

    @ColumnInfo(name = "screening_id")
    lateinit var screeningId: String

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readParcelable(Meta::class.java.classLoader)
    ) {
        id = parcel.readLong()
        syncPending = parcel.readByte() != 0.toByte()
        screeningId = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(body)
        parcel.writeParcelable(meta, flags)
        parcel.writeLong(id)
        parcel.writeByte(if (syncPending) 1 else 0)
        parcel.writeString(screeningId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DXARequest> {
        override fun createFromParcel(parcel: Parcel): DXARequest {
            return DXARequest(parcel)
        }

        override fun newArray(size: Int): Array<DXARequest?> {
            return arrayOfNulls(size)
        }
    }

}

data class DXABody(@Expose @field:SerializedName("dxa") var body: DXABodyData)
data class DXABodyData(
    @Expose @SerializedName(value = "comment") var comment: String? = null,
    @Expose @SerializedName(value = "device_id") var device_id: String? = null,
    @Expose @SerializedName(value = "whole_body") var whole_body: String? = null,
    @Expose @SerializedName(value = "lumbar_spine") var lumbar_spine: String? = null,
    @Expose @SerializedName(value = "hip") var hip: String? = null,
    @Expose @SerializedName(value = "hip_used") var hip_used: String? = null
)
